﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Goater
{
    internal class Relauncher
    {
        private const string AppDataFolder = "Goater";

        internal void Setup()
        {
            //Create app data folder
            //Copy over assembly

        }

        internal void Start()
        {
            //Check setup
            //Stat assembly with pid to monitor
            //Monitor assembly
            //Relaunch if assembly closed
        }

        private void CopyAssembly(string path)
        {
            
        }



        private void CheckAndCreateAppFolder()
        {
            var appDataPath = GetAppDataPath();
            if (!Directory.Exists(appDataPath)) Directory.CreateDirectory(appDataPath);
        }

        private string GetAppDataPath()
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appData, AppDataFolder);

            return path;
        }
    }
}
