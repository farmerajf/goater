﻿using System;
using System.Diagnostics;
using System.IO;

namespace Goater
{
    internal class TaskScheduler
    {
        private const string AppFolder = "Goater";
        private const string ScheduleFilename = "Schedule.xml";

        internal void Set()
        {
            CheckAndCreateAppFolder();
            
            var xml = GetTaskXml();
            SaveSchedule(xml, GetSchedulePath());

            SetTask(GetSchedulePath());
        }

        private void CheckAndCreateAppFolder()
        {
            var appDataPath = GetAppDataPath();
            if (!Directory.Exists(appDataPath)) Directory.CreateDirectory(appDataPath);
        }

        private string GetAppDataPath()
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appData, AppFolder);

            return path;
        }

        private string GetSchedulePath()
        {
            var path = Path.Combine(GetAppDataPath(), ScheduleFilename);
            return path;
        }

        private void SaveSchedule(string xml, string path)
        {
            var streamWriter = new StreamWriter(path);
            streamWriter.Write(xml);
            streamWriter.Close();
        }

        private void SetTask(string path)
        {
            var process = new Process {StartInfo = new ProcessStartInfo() {Arguments = "/create /XML " + path + " /tn Goater", FileName = @"c:\windows\system32\schtasks.exe"}};
            process.Start();
        }

        private string GetTaskXml()
        {
            var xml = "";
            xml += "<?xml version=\"1.0\" ?>";
            xml += "<Task xmlns=\"http://schemas.microsoft.com/windows/2004/02/mit/task\">";
            xml += "<RegistrationInfo>";
            xml += "<Date>2005-10-11T13:21:17-08:00</Date>";
            xml += "<Author>AuthorName</Author>";
            xml += "<Version>1.0.0</Version>";
            xml += "<Description>Notepad starts every day.</Description>";
            xml += "</RegistrationInfo>";
            xml += "<Triggers>";
            xml += "<CalendarTrigger>";
            xml += "<StartBoundary>2005-10-11T13:21:17-08:00</StartBoundary>";
            xml += "<EndBoundary>2006-01-01T00:00:00-08:00</EndBoundary>";
            xml += "<Repetition>";
            xml += "<Interval>PT1M</Interval>";
            xml += "<Duration>PT4M</Duration>";
            xml += "</Repetition>";
            xml += "<ScheduleByDay>";
            xml += "<DaysInterval>1</DaysInterval>";
            xml += "</ScheduleByDay>";
            xml += "</CalendarTrigger>";
            xml += "</Triggers>";
            xml += "<Principals>";
            xml += "<Principal>";
            xml += "<UserId>Administrator</UserId>";
            xml += "<LogonType>InteractiveToken</LogonType>";
            xml += "</Principal>";
            xml += "</Principals>";
            xml += "<Settings>";
            xml += "<Enabled>true</Enabled>";
            xml += "<AllowStartOnDemand>true</AllowStartOnDemand>";
            xml += "<AllowHardTerminate>true</AllowHardTerminate>";
            xml += "</Settings>";
            xml += "<Actions>";
            xml += "<Exec>";
            xml += "<Command>notepad.exe</Command>";
            xml += "</Exec>";
            xml += "</Actions>";
            xml += "</Task>";

            return xml;
        }
    }
}
